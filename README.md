
CHEAP SHEET


Prérequis

    Installation nodejs
        https://nodejs.org/en/

    Installation Ganache	
        https://trufflesuite.com/ganache/
        	Ganache, permet d'avoir une blockchain de test directement sur l'ordinateur

    Savoir créer un contrat intelligent
        https://remix.ethereum.org/



Utiliser la console Node avec Ganache et Web3.js

    Interagir avec la blockchain etheurun
        
        C:\laragon\www\javascript\blockchain\web3

        > mkdir web3

        créer un fichier package.json, stocke liste de libraries installé dans le projet.
            --yes, eviter les confirmations manuelles
        > npm init --yes

        installer librarie web3 (pour application node)
        > npm install --save web3

        afficher la console node
        > node

            importer la library web3
        > let Web3 = require('web3');

            verifier l'existance des composants
        > Web3 


    Connexion à ganache  - lier le projet au serveur ganache  via le webservice Web3
        > let web3 = new Web3(new Web3.providers.HttpProvider('HTTP://127.0.0.1:7545'));
        
        verifier l'existance des composants (différents comptes disponibles dans ganache, etc.)
        > web3


    Récuperer la balance, le nombre d'euther du premier compte  via le Web3

        consuler la balance en "wei" sur le compte 0xDfD205180BFA03182eaD2C6B0C57B03292B20670
        > web3.eth.getBalance('0xDfD205180BFA03182eaD2C6B0C57B03292B20670').then(console.log);
        
        consulter le balance en "euther" (convertion wei en euther)
        > web3.eth.getBalance('0xDfD205180BFA03182eaD2C6B0C57B03292B20670').then(res => { console.log(web3.utils.fromWei(res, "ether")); });


    Transferer vers une autre adresse
        > web3.eth.sendTransaction({from: '0xDfD205180BFA03182eaD2C6B0C57B03292B20670', to: '0xd484B763f0b650140148DbcE0F3ae1A6fa8C22c4', value: web3.utils.toWei("5","ether")});



Interagir avec un contrat intelligent


    Execution d'une fonction d'une smart contract depuis une app externe (node)
	source => NombreFavoris.sol
	exemple avec le contract 
			// SPDX-License-Identifier: MIT

	     		pragma solidity 0.8.7;

			contract NombreFavoris {

    				uint public nbrFavoris = 7;

    				function setNbrFavoris(uint _nbrFavoris) external {
        				nbrFavoris = _nbrFavoris;
    				}
			}
             
             from => l'adresse de la personne qui appelle la fonction(à récupérer dans ganache)
             to => l'adresse du smart contract(NombreFavoris)
	     data => l'adresse de la fonction(nbrFavoris) getter lié à l'input "nbrFavoris"
			créer une variable(nbrFavoris) en public => solidity crée automatiquement une fonction getter(nbrFavoris)

	obtenir le hash '0xf9bc83ec' à base du nom de la fonction(nbrFavoris) 
        > web3.utils.sha3('nbrFavoris()').substring(0,10);

        a l'aide d'une adresse(from),  on execute une fonction(data) dans un contrat intelligent(to)
    	> web3.eth.call({from: '0xDfD205180BFA03182eaD2C6B0C57B03292B20670', to: '0x645D611c46B768882272976D8a02774E59277Ca6', data: '0xf9bc83ec'}).then(console.log)
        ou 
	> web3.eth.call({from: '0xDfD205180BFA03182eaD2C6B0C57B03292B20670', to: '0x645D611c46B768882272976D8a02774E59277Ca6', data: web3.utils.sha3('nbrFavoris()').substring(0,10)}).then(console.log)


    Utiliser l'ABI, Application Binary Interface
		permet une communication plus aisée avec un contrat intelligent 
        > let ABI = [
		{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "_nbrFavoris",
				"type": "uint256"
			}
		],
		"name": "setNbrFavoris",
		"outputs": [],
		"stateMutability": "nonpayable",
		"type": "function"
		},
		{
		"inputs": [],
		"name": "nbrFavoris",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			}
		],
		"stateMutability": "view",
		"type": "function"
		}
	];

	> smart_contrat_adress = "0x645D611c46B768882272976D8a02774E59277Ca6";

        créer une instance d'un smart contrat
        > let contract = new web3.eth.Contract(ABI, smart_contrat_adress);

        changer la valeur d'un attribut d'un contrat dans une block chain
	> let caller_address = "0xDfD205180BFA03182eaD2C6B0C57B03292B20670"; // adresse de la personne dans ganache appelant la fonction
        > contract.methods.setNbrFavoris(500).send({from: caller_address}).then(console.log);

	appeler une fonction depuis une instance d'un contrat inteligent
	> contract.methods.nbrFavoris().call().then(console.log);



Web3.js pour interagir avec un contrat intelligent dans une page HTML
	// installer librarie web3 (pour application node)
	> npm install web3.js-browser

	> touch index.html
		inserer le code html en tapant sur les touches successives "!" + "Entree"
		intégrer la librarie dans l'entete dans la page html
	    		<script src="node_modules/web3.js-browser/build/web3.min.js"></script>
		intégrer dans le body
			    <div id="accounts"></div>
			    <div id="nbreFavoris"></div>

        	implémenter le script pour afficher tous les comptes existants(adresse)  depuis le local server ganache et la communication avec un contrat  intelligent
		<script>
        		let accounts = document.getElementById('accounts');
        		const init = async => {
            			// connect to  local serveur ganache
            			let web3 = new Web3(new Web3.providers.HttpProvider('HTTP://127.0.0.1:7545'));
            			// display all account from ganache
            			web3.eth.getAccounts().then(function(allAccounts){
                			for(let i = 0 ; i < allAccounts.length ; i++){
                    				accounts.innerHTML += allAccounts[i]+'<br />'
                			}
            			})
        		}

			let ABI = [
                {
                "inputs": [
                    {
                        "internalType": "uint256",
                        "name": "_nbrFavoris",
                        "type": "uint256"
                    }
                ],
                "name": "setNbrFavoris",
                "outputs": [],
                "stateMutability": "nonpayable",
                "type": "function"
                },
                {
                "inputs": [],
                "name": "nbrFavoris",
                "outputs": [
                    {
                        "internalType": "uint256",
                        "name": "",
                        "type": "uint256"
                    }
                ],
                "stateMutability": "view",
                "type": "function"
                }
            ];

            let SMART_CONTRAT_ADDRESS = "0x645D611c46B768882272976D8a02774E59277Ca6";

            let contract = new web3.eth.Contract(ABI, SMART_CONTRAT_ADDRESS);

            (async function getNbrFavoris() {
                const leNbreFavoris = await contract.methods.nbrFavoris().call()
                .then(receipt =>{
                    console.log(receipt);
                    //nbrFavoris.innerHTML = receipt;
                })
                .catch(error =>{
                    alert('erreur !')
                })
            })();

        		init();
    		</script>
            



